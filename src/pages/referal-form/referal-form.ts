import {
  NavController,
  LoadingController,
  AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Data } from '../../providers/data';
import { ThankYouPage } from '../thank-you/thank-you';
import { EmailValidator } from '../../validators/email';
import { NinjaTipsPage } from '../pages/ninja-tips/ninja-tips';

@Component({
  selector: 'page-referal-form',
  templateUrl: 'referal-form.html',
})
export class ReferalFormPage {
  public referForm;
  emailChanged: boolean = false;
  passwordChanged: boolean = false;
  submitAttempt: boolean = false;
  loading: any;
  nameChanged = false

  constructor(public nav: NavController, public data: Data,
    public formBuilder: FormBuilder, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {

    this.referForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
    });
  }

  elementChanged(input){
    let field = input.inputControl.name;
    this[field + "Changed"] = true;
  }

  applyNow(){
    this.submitAttempt = true;

    if (!this.referForm.valid){
      console.log(this.referForm.value);
    } else {
      this.data.referFriend(this.referForm.value).then( authData => {
        this.nav.push(ThankYouPage);
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });

      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }
  }
}
