import {
  NavController,
  LoadingController,
  AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Data } from '../../providers/data';
import { EmailValidator } from '../../validators/email';
import { ThankYouPage } from '../thank-you/thank-you';
import { AboutUsPage } from '../about-us/about-us';
import { PolicyPage } from '../pages/policy/policy';
@Component({
  selector: 'page-application-form',
  templateUrl: 'application-form.html',
})
export class ApplicationFormPage {
  public applyForm;
  emailChanged: boolean = false;
  nameChanged: boolean = false;
  passwordChanged: boolean = false;
  submitAttempt: boolean = false;
  loading: any;

  constructor(public nav: NavController, public data: Data,
    public formBuilder: FormBuilder, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {

    this.applyForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      phone: ['', Validators.compose([Validators.minLength(10), Validators.required])]
    });
  }

  elementChanged(input){
    let field = input.inputControl.name;
    this[field + "Changed"] = true;
  }

  applyNow(){
    this.submitAttempt = true;

    if (!this.applyForm.valid){
      console.log(this.applyForm.value);
    } else {
      this.data.applyForCourse(this.applyForm.value).then( authData => {
        this.nav.push(ThankYouPage);
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });

      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }
  }
}
