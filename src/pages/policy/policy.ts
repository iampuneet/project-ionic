import { Component, NgZone } from '@angular/core';
import firebase from 'firebase';

import { NavController } from 'ionic-angular';
import { AuthData } from '../../providers/auth-data';
import { Data } from '../../providers/data';
import { LoginPage } from '../login/login';
import { BookmarksPage } from '../bookmarks/bookmarks';
import { TutorialsPage } from '../tutorials/tutorials';
import { TutorialsFilterPage } from '../tutorials-filter/tutorials-filter';

/*
  Generated class for the PolicyPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-policy',
  templateUrl: 'policy.html'
})
export class PolicyPage {

  constructor(public navCtrl: NavController, public authData: AuthData, public data: Data, private zone: NgZone) {
    firebase.auth().onAuthStateChanged( user => {
      if (!user) {
        this.navCtrl.setRoot(LoginPage);
        console.log("There's not a logged in user");
      }
    });
  }

  ionViewDidLoad() {
    console.log('Hello PolicyPage Page');
  }

  logMeOut() {
    this.authData.logoutUser().then( () => {
      this.navCtrl.setRoot(LoginPage);
    });
  }


}
