import { Component , NgZone } from '@angular/core';
import { NavController } from 'ionic-angular';

import firebase from 'firebase';
import { AuthData } from '../../providers/auth-data';
import { Data } from '../../providers/data';
import { LoginPage } from '../login/login'
/*
  Generated class for the NinjaTipsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-ninja-tips',
  templateUrl: 'ninja-tips.html'
})
export class NinjaTipsPage {

  pageData;
  pageHeader : String;
  pageDescription : String;
  successCallBack : String;
  cancelCallBack : String;
  successCallback = function(success) {
    alert('payment_id: ' + success.razorpay_payment_id)
    var orderId = success.razorpay_order_id
    var signature = success.razorpay_signature
  }
  
  cancelCallback = function(error) {
    alert(error.description + ' (Error '+error.code+')')
  }
  constructor(public navCtrl: NavController,private authData : AuthData,private data : Data,private zone : NgZone) {

    firebase.auth().onAuthStateChanged(user => {
      if(!user){
        this.navCtrl.setRoot(LoginPage);
      }
      else{
        this.fetchData();
        console.log("fetch tutorials");  
      }
    })
  }

  fetchData(){
    this.data.getNinjaTips().then((data)=>{
        this.pageData = data;
        console.log('--> Page Data '+ this.pageData.pageData.name);
        this.pageHeader = this.pageData.pageData.name;      
        this.pageDescription = this.pageData.pageData.description;
        this.zone.run( () => {
          console.log('running zone');
        })
    })
  }

  pay() {
    var options = {
      description: 'Master Ninja Package',
      image: 'https://firebasestorage.googleapis.com/v0/b/tutor-app-48dcb.appspot.com/o/logo.png?alt=media&token=fe2143e9-565c-4a50-8b22-2bcc885dfa6c',
      currency: 'INR',
      key: 'rzp_live_qeKPgMSjhsvAXU',
      amount: '50000',
      name: 'Smart Marketing Tribe',
      theme: {
        color: '#F37254'
      },
      modal: {
        ondismiss: function() {
          alert('dismissed')
        }
      }
    };

    var successCallback = function(payment_id) {
      alert('payment_id: ' + payment_id);
    };

    var cancelCallback = function(error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }

 



  logMeOut() {
    this.authData.logoutUser().then( () => {
      this.navCtrl.setRoot(LoginPage);
    });
  }

  ionViewDidLoad() {
    console.log('Hello NinjaTipsPage Page');
  }

}
