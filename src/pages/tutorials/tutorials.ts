import { Component, NgZone } from '@angular/core';
import firebase from 'firebase';

import { NavController } from 'ionic-angular';
import { AuthData } from '../../providers/auth-data';
import { Data } from '../../providers/data';
import { LoginPage } from '../login/login';
import { NinjaTipsPage } from '../pages/ninja-tips/ninja-tips';
@Component({
  selector: 'page-tutorials',
  templateUrl: 'tutorials.html'
})
export class TutorialsPage {
  items= []
  rate = 4.2
  todayDate = new Date().toJSON().slice(0,10);
  key: any

  constructor(public navCtrl: NavController, public authData: AuthData, public data: Data, private zone: NgZone) {
    firebase.auth().onAuthStateChanged( user => {
      if (!user) {
        this.navCtrl.setRoot(LoginPage);
        console.log("There's not a logged in user");
      } else {
        this.data.getTutorials(this.key).then((data) => {
          this.items = data.items;
          this.key = data.key;
          this.data.getBookmarksList().then(() => {
            this.zone.run(() => {
              console.log('running zone');
            });
          });
        });
      }
    });
  }

  logMeOut() {
    this.authData.logoutUser().then( () => {
      this.navCtrl.setRoot(LoginPage);
    });
  }

  ratingUpdated() {
    console.log('Rating updated')
    console.log(this.rate)
    this.rate = 4.2
  }

  bookmark(key) {
    this.data.updateBookmark(key);
    this.data.getBookmarksList();
  }

  filterValues(value) {
    console.log(value);
    console.log(value);
    this.data.filterTutorials(value).then((data) => {
      this.items = data;
    });
  }

  doInfinite(infiniteScroll) {
    this.data.getTutorials(this.key).then((data) => {
      this.items = data.items;
      this.key = data.key;
      this.data.getBookmarksList().then(() => {
        this.zone.run(() => {
          console.log('running zone');
        });
      });

      infiniteScroll.complete();
    });
  }

  doRefresh(refresher) {
    this.data.resetTutorialList().then((data) => {
      this.key = null;
      this.data.getTutorials(this.key).then((data) => {
        this.items = data.items;
        this.key = data.key;
        this.data.getBookmarksList().then(() => {
          this.zone.run(() => {
            console.log('running zone');
            refresher.complete();
          });
        });
      });
    });
  }
}
