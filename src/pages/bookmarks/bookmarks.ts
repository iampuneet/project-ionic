import { Component, NgZone } from '@angular/core';
import firebase from 'firebase';

import { NavController } from 'ionic-angular';
import { AuthData } from '../../providers/auth-data';
import { Data } from '../../providers/data';
import { LoginPage } from '../login/login';
import { PolicyPage } from '../pages/policy/policy';
@Component({
  selector: 'page-bookmarks',
  templateUrl: 'bookmarks.html'
})
export class BookmarksPage {
  items= []
  rate = 4.2
  todayDate = new Date().toJSON().slice(0,10);
  key: any

  constructor(public navCtrl: NavController, public authData: AuthData, public data: Data, private zone: NgZone) {
    firebase.auth().onAuthStateChanged( user => {
      if (!user) {
        this.navCtrl.setRoot(LoginPage);
        console.log("There's not a logged in user");
      } else {
        this.fetchBookmarks();
      }
    });
  }

  fetchBookmarks() {
    this.data.getBookmarkTutorials().then((items) => {
      this.items = items;
      this.zone.run(() => {
        console.log('running zone');
      });
    });
  }

  logMeOut() {
    this.authData.logoutUser().then( () => {
      this.navCtrl.setRoot(LoginPage);
    });
  }

  ratingUpdated() {
    console.log('Rating updated')
    console.log(this.rate)
    this.rate = 4.2
  }

  bookmark(key) {
    this.data.updateBookmark(key);
    this.data.getBookmarksList();
  }

  doRefresh(refresher) {
    this.data.getBookmarkTutorials().then((items) => {
      this.items = items;
      this.zone.run(() => {
        refresher.complete();
      });
    });
  }
}
