import { Component, NgZone } from '@angular/core';
import firebase from 'firebase';

import { NavController } from 'ionic-angular';
import { AuthData } from '../../providers/auth-data';
import { Data } from '../../providers/data';
import { LoginPage } from '../login/login';
import { BookmarksPage } from '../bookmarks/bookmarks';
import { TutorialsPage } from '../tutorials/tutorials';
import { TutorialsFilterPage } from '../tutorials-filter/tutorials-filter';
import { AboutUsPage } from '../about-us/about-us';
import { PolicyPage } from '../pages/policy/policy';
import { NinjaTipsPage } from '../pages/ninja-tips/ninja-tips';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  items= []
  rate = 4.2
  todayDate = new Date().toJSON().slice(0,10);
  bookmarksPageRoot = BookmarksPage;
  tutorialsPageRoot = TutorialsPage;
  tutorialsFilterPageRoot = TutorialsFilterPage;

  constructor(public navCtrl: NavController, public authData: AuthData, public data: Data, private zone: NgZone) {
    firebase.auth().onAuthStateChanged( user => {
      if (!user) {
        this.navCtrl.setRoot(LoginPage);
        console.log("There's not a logged in user");
      }
    });
  }

  logMeOut() {
    this.authData.logoutUser().then( () => {
      this.navCtrl.setRoot(LoginPage);
    });
  }
}
