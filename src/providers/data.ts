import { Injectable } from '@angular/core';
import firebase from 'firebase';
import _ from 'lodash';
import moment from 'moment';

@Injectable()
export class Data {
  public userProfile: any;
  public fireData: any;
  public items: any;
  private user: any;
  public pageData : any;

  constructor() {
    this.userProfile = firebase.database().ref('/userProfile');
    this.fireData = firebase.database();
    this.items = [];
    firebase.auth().onAuthStateChanged(user => {
      this.user = user;
    });
  }



  /**
   * [resetTutorialList - resets the list of tutorials]
   *
   */
   resetTutorialList(): any{
     return new Promise((resolve, reject)  => {
       this.items = [];
       resolve([]);
     });
   }


   


  /**
   * [getTutorials description]
   *
   * @param  {string} key    [Last key]
   */
  getTutorials(key): any {
    console.log('getTutorials')
    return new Promise((resolve, reject)  => {
      firebase.auth().onAuthStateChanged( user => {
        if (user) {
          if (key) {
            console.log('with key');
            this.fireData.ref('/tutorials').endAt(null, key).limitToLast(5).once('value').then((snapshot) => {
              console.log(snapshot.val())
              const data = snapshot.val();
              console.log(data);
              for (var key in data) {
                if (data.hasOwnProperty(key) && data[key]) {
                  data[key]['key'] = key;
                  data[key]['bookmarked'] = false;
                  this.items.push(data[key]);
                }
              }

              this.items = _.sortBy(this.items, ['sortingTime']);
              this.items = _.uniqBy(this.items, 'key');
              resolve({
                items: this.items,
                key: this.items[this.items.length - 1].key
              });
            });
          } else {
            console.log('without key');
            this.fireData.ref('/tutorials').limitToLast(5).once('value').then((snapshot) => {
              console.log(snapshot.val())
              const data = snapshot.val();
              console.log(data);
              for (var key in data) {
                if (data.hasOwnProperty(key) && data[key]) {
                  data[key]['key'] = key;
                  data[key]['bookmarked'] = false;
                  this.items.push(data[key]);
                }
              }

              this.items = _.sortBy(this.items, ['sortingTime']);
              this.items = _.uniqBy(this.items, 'key');
              const item = this.items.pop();

              resolve({
                items: this.items,
                key: item.key
              });
            });
          }
        } else {
          reject('Not Authorized');
        }
      });
    });
  }

  /**
   * [updateBookmark description]
   *
   */
  updateBookmark(key: string): any {
    const refValue = `/bookmarks/${this.user.uid}/${key}`;
    console.log(refValue);
    const value = {
      bookmarked: true,
      updated: moment().unix(),
    };

    this.fireData.ref(refValue).update(value);
  }



getMasterTips() : any{
  return new Promise((resolve,reject) => {
  firebase.auth().onAuthStateChanged( user => {
    if (user) {
        console.log('in to getting the ');
        this.fireData.ref('/sublevels').orderByChild('name').equalTo('THE MASTERMIND').limitToLast(5).once('value').then((snapshot) => {
          console.log(snapshot.val())
          const data = snapshot.val();
          console.log(data);
          
          for (var key in data) {
              this.pageData = data[key];
          } 
          console.log(this.pageData.name+'page data in service');
          console.log(this.pageData.description+'page data in service');
          console.log(this.pageData.amount+'page data in service');

          resolve({
            pageData: this.pageData
          });
        });
      
    } else {
      reject('Not Authorized');
    }
  });
});
}


getNinjaMasterTips() : any{
  return new Promise((resolve,reject) => {
  firebase.auth().onAuthStateChanged( user => {
    if (user) {
        console.log('in to getting the ');
        this.fireData.ref('/sublevels').orderByChild('name').equalTo('Ninja Tips + Mastermind').limitToLast(5).once('value').then((snapshot) => {
          console.log(snapshot.val())
          const data = snapshot.val();
          console.log(data);
          
          for (var key in data) {
              this.pageData = data[key];
          } 
          console.log(this.pageData.name+'page data in service');
          console.log(this.pageData.description+'page data in service');
          console.log(this.pageData.amount+'page data in service');

          resolve({
            pageData: this.pageData
          });
        });
      
    } else {
      reject('Not Authorized');
    }
  });
});
}


getNinjaTips() : any{
  return new Promise((resolve,reject) => {
  firebase.auth().onAuthStateChanged( user => {
    if (user) {
        console.log('in to getting the ');
        this.fireData.ref('/sublevels').orderByChild('name').equalTo('Ninja Tips').limitToLast(5).once('value').then((snapshot) => {
          console.log(snapshot.val())
          const data = snapshot.val();
          console.log(data);
          
          for (var key in data) {
              this.pageData = data[key];
          } 
          console.log(this.pageData.name+'page data in service');
          console.log(this.pageData.description+'page data in service');
          console.log(this.pageData.amount+'page data in service');

          resolve({
            pageData: this.pageData
          });
        });
      
    } else {
      reject('Not Authorized');
    }
  });
});
}

  /**
   * [getBookmarksList description]
   *
   */
   getBookmarksList(): any{
     return new Promise((resolve, reject)  => {
       const refValue = `/bookmarks/${this.user.uid}`
       this.fireData.ref(refValue).once('value').then((snapshot) => {
         // TODO - Optimize it
         const bookmarks = snapshot.val();
         this.items.forEach((item, index) => {
           for (var key in bookmarks) {
             if (bookmarks.hasOwnProperty(key) && bookmarks[key] && (this.items[index].key === key)) {
               this.items[index].bookmarked = true;
             }
           }
         });

         resolve(bookmarks);
       });
     });
   }



   

   /**
    * [applyForCourse description]
    *
    */
   applyForCourse(data): any {
     return new Promise((resolve, reject)  => {
       console.log('applied');
       const refValue = '/applications';
       const value = {
         name: data.name,
         phone: data.phone,
         email: data.email,
         userId: this.user.uid,
         updated: moment().unix(),
       };

       this.fireData.ref(refValue).push(value);
       resolve('done');
     });
   }


   /**
    * [referFriend description]
    *
    */
   referFriend(data): any {
     return new Promise((resolve, reject)  => {
       console.log('applied');
       const refValue = '/referal';
       const refTaskValue = '/referalTasks/tasks';
       const value = {
         name: data.name,
         email: data.email,
         userId: this.user.uid,
         updated: moment().unix(),
       };

       this.fireData.ref(refValue).push(value);
       this.fireData.ref(refTaskValue).push(value);
       resolve('done');
     });
   }

   /**
    * [getBookmarksTutorials - gets the list of bookmars with values]
    *
    */
    getBookmarkTutorials(): any{
      console.log('bookmarks');
      return new Promise((resolve, reject)  => {
        const items = [];
        const itemsPromise = [];
        const refValue = `/bookmarks/${this.user.uid}`;

        this.fireData.ref(refValue).once('value')
        .then((snapshot) => {
          const bookmarks = snapshot.val();
          for (var key in bookmarks) {
            if (bookmarks.hasOwnProperty(key) && bookmarks[key]) {
              itemsPromise.push(this.fireData.ref(`/tutorials/${key}`).once('value'))
            }
          }

          return Promise.all(itemsPromise);
        })
        .then((itemList) => {
          itemList.forEach((data) => {
            const item = data.val();
            item.bookmarked = true;

            items.push(item);
          });

          this.items = _.sortBy(this.items, ['savedTime']);
          resolve(items);
        })
        .catch((err) => reject(err));
      });
    }

   /**
    * [filterTutorials description]
    *
    */
   filterTutorials(date: any): any{
     return new Promise((resolve, reject)  => {
      //  this.fireData.ref('/tutorials').orderByChild('savedTime').startAt(moment(date).startOf('day').unix()).endAt(moment(date).endOf('day').unix()).limitToLast(5).once('value').then((snapshot) => {
       this.fireData.ref('/tutorials').orderByChild('savedTime').endAt(moment(date).endOf('day').unix()).limitToLast(5).once('value').then((snapshot) => {
         console.log('test filterTutorials');
         console.log(snapshot.val())
         const data = snapshot.val();
         if (data) {
           this.items = [];
           for (var key in data) {
             if (data.hasOwnProperty(key) && data[key]) {
               data[key]['key'] = key;
               data[key]['bookmarked'] = false;
               this.items.push(data[key]);
             }
           }

           this.items = _.sortBy(this.items, ['sortingTime']);
           resolve({
             items: this.items,
             key: this.items[this.items.length - 1].key
           });
         } else {
           resolve({
             items: [],
             key: null
           });
         }
       });
     });
   }
}
