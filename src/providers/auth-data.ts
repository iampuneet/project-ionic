import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { Facebook } from 'ionic-native';
import { GooglePlus } from 'ionic-native';
import { TwitterConnect } from 'ionic-native';

@Injectable()
export class AuthData {
  // Here we declare the variables we'll be using.
  public fireAuth: any;
  public userProfile: any;

  constructor() {
    this.fireAuth = firebase.auth(); // We are creating an auth reference.
    // This declares a database reference for the userProfile/ node.
    this.userProfile = firebase.database().ref('/userProfile');
  }

  /**
   * [loginUser We'll take an email and password and log the user into the firebase app]
   * @param  {string} email    [User's email address]
   * @param  {string} password [User's password]
   */
  loginUser(email: string, password: string): any {
    return this.fireAuth.signInWithEmailAndPassword(email, password);
  }

  /**
   * [signupUser description]
   * This function will take the user's email and password and create a new account on the Firebase app, once it does
   * it's going to log the user in and create a node on userProfile/uid with the user's email address, you can use
   * that node to store the profile information.
   * @param  {string} email    [User's email address]
   * @param  {string} password [User's password]
   */
  signupUser(email: string, password: string): any {
    return this.fireAuth.createUserWithEmailAndPassword(email, password).then((newUser) => {
      this.userProfile.child(newUser.uid).set({
          email: email
      });
    });
  }

  /**
   * [resetPassword description]
   * This function will take the user's email address and send a password reset link, then Firebase will handle the
   * email reset part, you won't have to do anything else.
   *
   * @param  {string} email    [User's email address]
   */
  resetPassword(email: string): any {
    return this.fireAuth.sendPasswordResetEmail(email);
  }

  /**
   * This function doesn't take any params, it just logs the current user out of the app.
   */
  logoutUser(): any {
    return this.fireAuth.signOut();
  }

  signupGoogle() {
    return new Promise((resolve, reject) => {
      GooglePlus.login({
        'webClientId': '864786055549-fu5lgh92idp94aiglmhqvfsc9d321s8d.apps.googleusercontent.com',
      })
      .then((userData) => {
         var provider = firebase.auth.GoogleAuthProvider.credential(userData.idToken);

         return firebase.auth().signInWithCredential(provider);
      })
      .then((success) => {
          this.userProfile = success;
          resolve(success);
      })
      .catch((error) => {
          console.log("Firebase failure: " + JSON.stringify(error));
          reject(error);
      });
    });
  }

  signupFacebook() {
    return new Promise((resolve, reject)  => {
      Facebook.login(['email']).then( (response) => {
        let facebookCredential = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);

        return firebase.auth().signInWithCredential(facebookCredential);
      })
      .then((success) => {
        console.log("Firebase success: " + JSON.stringify(success));
        this.userProfile = success;
        resolve(success);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  }

  signupTwitter() {
    return new Promise((resolve, reject) => {
      TwitterConnect.login().then((response) => {
        let twitterCredentials = firebase.auth.TwitterAuthProvider.credential(response.token, response.secret);

        firebase.auth().signInWithCredential(twitterCredentials)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
        console.log(JSON.stringify(response));
      }, (error) => {
        console.log(error);
        reject(error);
      });
    });
  }
}
