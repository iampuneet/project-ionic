import { Component } from '@angular/core';
import { Platform, MenuController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ApplicationFormPage } from '../pages/application-form/application-form';
import { ReferalFormPage } from '../pages/referal-form/referal-form';
import { AboutUsPage } from '../pages/about-us/about-us'
import { PolicyPage } from '../pages/policy/policy';
import { NinjaTipsPage } from '../pages/ninja-tips/ninja-tips'

import { MastermindPage } from '../pages/mastermind/mastermind';
import firebase from 'firebase';
import { MasterNinjaPage } from '../pages/master-ninja/master-ninja';


@Component({
  template: `
    <ion-menu [content]="content">
      <ion-header>
        <ion-toolbar color="primary">
          <ion-title>Menu</ion-title>
        </ion-toolbar>
      </ion-header>
      <ion-content>
        <ion-list>
          <button ion-item menuToggle (click)="openPage(homePage)">
            Free Videos
          </button>
          <ion-item-divider>
          <button ion-button item-end>Basic Package</button>
          </ion-item-divider>
          <button ion-item menuToggle (click)="openPage(ninjaTipsPage)">
          Ninja Tips
          </button>
          <ion-item-divider>
          <button ion-button item-end>Advanced Package</button>
          </ion-item-divider>
          <button ion-item menuToggle (click)="openPage(mastermindPage)">
          THE MASTERMIND
          </button>
          <button ion-item menuToggle (click)="openPage(masterNinjaPage)">
          NINJA + MASTERMIND
          </button>
        </ion-list>
        <ion-list class="logout-list">
          <button ion-item menuToggle (click)="openPage(aboutUsPage)">
            About us
          </button>
          <button ion-item menuToggle (click)="openPage(policyPage)">
            Policy
          </button>
          <button ion-item menuToggle (click)="openPage(referalPage)">
            Refer a friend
          </button>
          <button ion-item menuToggle (click)="logMeOut()">
            Logout
          </button>
        </ion-list>
      </ion-content>
    </ion-menu>
    <ion-nav #content [root]="rootPage"></ion-nav>
  `
})
export class MyApp {
  rootPage: any = HomePage;
  loginPage: any = LoginPage;
  homePage: any = HomePage;
  applyPage: any = ApplicationFormPage;
  referalPage: any = ReferalFormPage;
  aboutUsPage : any = AboutUsPage;
  policyPage : any = PolicyPage;
  ninjaTipsPage : any = NinjaTipsPage;
  mastermindPage : any = MastermindPage;
  masterNinjaPage : any = MasterNinjaPage;
  constructor(platform: Platform, menu: MenuController) {
    const config = {
      apiKey: "AIzaSyA97h6-CFIjtc7E2WgJJpezs5450E_QRcA",
      authDomain: "tutor-app-48dcb.firebaseapp.com",
      databaseURL: "https://tutor-app-48dcb.firebaseio.com",
      storageBucket: "tutor-app-48dcb.appspot.com",
      messagingSenderId: "864786055549"
    };
    firebase.initializeApp(config);

    firebase.auth().onAuthStateChanged( user => {
      if (!user) {
        this.rootPage = LoginPage;
        console.log("There's not a logged in user!");
        menu.enable(false);
      } else {
        menu.enable(true);
      }
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    this.rootPage = page;
  }

  logMeOut() {
    firebase.auth().signOut().then( () => {
      this.rootPage = LoginPage;
    });
  }
}
