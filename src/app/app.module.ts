import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Ionic2RatingModule } from 'ionic2-rating';

// Import Pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { BookmarksPage } from '../pages/bookmarks/bookmarks';
import { TutorialsPage } from '../pages/tutorials/tutorials';
import { TutorialsFilterPage } from '../pages/tutorials-filter/tutorials-filter';
import { ApplicationFormPage } from '../pages/application-form/application-form';
import { ReferalFormPage } from '../pages/referal-form/referal-form';
import { ThankYouPage } from '../pages/thank-you/thank-you';
import { AboutUsPage } from '../pages/about-us/about-us'
import { PolicyPage } from '../pages/policy/policy';



// Import Providers
import { AuthData } from '../providers/auth-data';
import { Data } from '../providers/data';
import {SafePipe} from '../lib/safe';
import { MomentDate } from '../lib/moment-date';
import { NinjaTipsPage } from '../pages/ninja-tips/ninja-tips';
import { MastermindPage } from '../pages/mastermind/mastermind'
import { MasterNinjaPage } from '../pages/master-ninja/master-ninja';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    ResetPasswordPage,
    BookmarksPage,
    TutorialsPage,
    TutorialsFilterPage,
    ApplicationFormPage,
    ReferalFormPage,
    ThankYouPage,
    SafePipe,
    MomentDate,
    AboutUsPage,
    PolicyPage,
    NinjaTipsPage,    
    MastermindPage,
    MasterNinjaPage,
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    ResetPasswordPage,
    BookmarksPage,
    TutorialsPage,
    TutorialsFilterPage,
    ApplicationFormPage,
    ReferalFormPage,
    ThankYouPage,
    AboutUsPage,
    PolicyPage,
    NinjaTipsPage,
    MastermindPage,
    MasterNinjaPage,
  ],
  providers: [
    AuthData,
    Data
  ]
})
export class AppModule {}
